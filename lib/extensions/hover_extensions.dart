import 'package:flutter/material.dart';
import 'dart:html' as html;
import 'package:portfolio/widgets/fade_in_out_OnHover.dart';
import 'package:portfolio/widgets/translate_on_hover.dart';

extension hoverExtension on Widget {

  static final appContainer = html.window.document.getElementById('app-container');

  Widget get showCursorOnHover {
    return MouseRegion(
      child: this,
      onHover: (event){
        appContainer.style.cursor = 'pointer';
      },

      onExit: (event){
        appContainer.style.cursor = 'default';
      },
    );
  }

  Widget get fadeInOutOnHover{
    return FadeInOutOnHover(
      child: this,
    );
  }

  Widget get translateOnHover{
    return TranslateOnHover(
      child: this,
    );
  }
}