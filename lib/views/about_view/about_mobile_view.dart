import 'package:flutter/material.dart';
import 'package:portfolio/widgets/centered_view.dart';
import 'package:portfolio/widgets/image_slider.dart';
import 'package:portfolio/widgets/topic_of_view.dart';


class AboutMobileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TopicOfView(
          title: 'ABOUT ME',
          numberTile: '01',
          fontSize: 30,
        ),

        SizedBox(height: 50,),

        CenteredView(
          horizontalPadding: 40,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children:<Widget>[
              Text('Sampang',
                style: TextStyle(fontSize:18, color: Colors.black, letterSpacing: 6, fontWeight: FontWeight.w500),),
              SizedBox(height: 4,),
              Text('Bidhan Rai',
                style: TextStyle(fontSize: 18, color: Colors.black, letterSpacing: 2, fontWeight: FontWeight.w500),),
              SizedBox(height: 20,),
              Text("My name is Bidhan. Born in 1997 based in Kathmandu, Nepal. Graduated from Kathmandu University (2019) "
                      "B.E in Computer Engineering. Recently working as a Flutter Developer in an IT firm in Kathmandu. I love being close to nature, travel, "
                      "play video games, football, love to spend time with my friends and family and somtimes immensely enjoy doing nothing. "
                      "I am not much of a thinker instead enjoy being random, trying new things, constantly learning.", style: TextStyle(height: 2, fontWeight: FontWeight.w100),),
              // SizedBox(height: 50,),
              ImageSlider()
            ]
          ),
        )
      ],
    );
  }
}