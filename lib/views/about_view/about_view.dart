import 'package:flutter/material.dart';
import 'package:portfolio/views/about_view/about_mobile_view.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'about_desktop_view.dart';

class AboutView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return ScreenTypeLayout(
     mobile: AboutMobileView(),
     desktop: AboutDesktopView(),
   );
  }
}