import 'package:flutter/material.dart';
import 'package:portfolio/constants/app_color.dart';
import 'package:portfolio/widgets/launch_url.dart';
import 'package:portfolio/widgets/navigation_bar/nav_bar_item.dart';
import 'package:responsive_builder/responsive_builder.dart';


class ContactView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
    
      builder: (context, sizingInformation){
          double fontSize = sizingInformation.isMobile?12:20;
        return SizedBox(
          height: 220,
            child: Container(
            decoration: BoxDecoration(
              gradient: new LinearGradient(
                colors: [purple, pink],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                tileMode: TileMode.repeated
              ),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
              
                children:[
                  NavBarItem(
                    child: InkWell(
                      onTap: (){
                        final Uri _emailLaunchUri = Uri(
                          scheme: 'mailto',
                          path: 'bidhanrai424@gmail.com',
                          queryParameters: {
                            'subject': 'Hello there'
                          },
                        );

                        // mailto:smith@example.com?subject=Example+Subject+%26+Symbols+are+allowed%21
                       launchUrl(sizingInformation.isDesktop?'https://mail.google.com/mail/u/0/#inbox?compose=new':_emailLaunchUri.toString());
                      },
                      child: Text('bidhanrai424@gmail.com',
                        style: TextStyle(letterSpacing: 4, color: Colors.white,fontSize: fontSize),),
                    )
                  ),
                  SizedBox(height: sizingInformation.isMobile?14:30,),
                  Text('Made with Flutter', style: TextStyle(letterSpacing: 2, color: Colors.white,fontSize: fontSize),)
                ]
              ),
            ),
          ),
        );
      },
    );
  }
}