import 'package:flutter/material.dart';
import 'package:portfolio/constants/app_color.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:portfolio/extensions/hover_extensions.dart';
import 'package:fluttertoast/fluttertoast.dart';


class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder:(context, sizingInformation){
        double leftTextPosition = sizingInformation.isMobile?40.0:sizingInformation.isTablet?100.0: 165.0; 
        double imageWidth = sizingInformation.isMobile? MediaQuery.of(context).size.width: MediaQuery.of(context).size.width*0.6; 
        double imageHeight = sizingInformation.isMobile? MediaQuery.of(context).size.height*0.65: MediaQuery.of(context).size.height; 
        double scalingFactor = sizingInformation.isMobile? 0.5: 1;  
       
        return Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          color: Colors.black,
          child: Stack(
            alignment: sizingInformation.isMobile?Alignment.bottomCenter: Alignment.center,
            children:[
              Positioned(
                right: 0,
                top: 0,
                child: Image.asset('assets/computer.jpg',fit: BoxFit.cover,
                  width: imageWidth,
                  height: imageHeight,
                ),
              ),

              Positioned(
                left: leftTextPosition,
                bottom: leftTextPosition,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Hello There!', style: TextStyle(fontSize: 60 * scalingFactor, color: Colors.white, letterSpacing: 8 * scalingFactor),),
                    SizedBox(height: 10 *scalingFactor,),

                    Text('Welcome to my website', style: TextStyle(fontSize: 18 * scalingFactor, color: Colors.white, letterSpacing: 2 * scalingFactor),),
                    SizedBox(height: 20 *scalingFactor,),

                    SizedBox(
                      width: 100 * scalingFactor,
                      child: Divider( color: homePageWidgetColor,thickness: 4 * scalingFactor,)
                    ),
                    SizedBox(
                      width: 150 * scalingFactor,
                      child: Divider( color: homePageWidgetColor, thickness: 4 * scalingFactor, indent: 50 * scalingFactor,)
                    ),
                    SizedBox(height: 20 *scalingFactor,),
                    Text('I love Design, Technology,\nand Story.', 
                      style: TextStyle(color: Colors.white, fontSize: 28 * scalingFactor, wordSpacing: 8 * scalingFactor, height: 1.5)),

                    SizedBox(height: 30 *scalingFactor,),

                    FlatButton(
                      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      color: homePageWidgetColor,
                      onPressed: (){
                        Fluttertoast.showToast(
                          msg: "Comming soon",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0,
                          webShowClose: true,
                          webPosition: 'center'
                      );
                      }, 
                      child: Text('Services', style: TextStyle(color: Colors.white, fontSize: 16, letterSpacing: 2),),
                    ).translateOnHover
                  ],
                ),
              ),
                
            
            ]
          ),
        );
      }
    );
  }
}