import 'package:flutter/material.dart';
import 'package:portfolio/views/about_view/about_view.dart';
import 'package:portfolio/views/contact_view/contact_view.dart';
import 'package:portfolio/views/home_view/home_view.dart';
import 'package:portfolio/views/passion/passion_view.dart';
import 'package:portfolio/views/skill_view/skill_view.dart';
import 'package:portfolio/widgets/navigation_bar/navigation_bar.dart';


class LayoutTemplate extends StatelessWidget {

  static ScrollController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        alignment: Alignment.topCenter,
        children:[
          ListView(
            controller: controller,
            
              children:<Widget>[
                HomeView(),
                SizedBox(height: 200),
                AboutView(),
                SizedBox(height: 200),
                PassionView(),
                SizedBox(height: 200),
                SkillView(),
                SizedBox(height: 200),
                ContactView()
              ],            
             
          ),

          NavigationBar()
          
        ]
      ),
    );
  }
}