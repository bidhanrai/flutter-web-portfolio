import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class PassionContent extends StatelessWidget {

  final String assetPath;
  final String title;
  final String description;
  PassionContent({this.title, this.assetPath, this.description});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation){
        double textHeight = sizingInformation.isMobile?1.9:2.5;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children:<Widget>[
            Image.asset(assetPath,),
            SizedBox(height: 10,),
            Text(title, style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500, letterSpacing: 4),),
            SizedBox(height: 20,),
            Text(description, style: TextStyle(height: textHeight, fontWeight: FontWeight.w100),),
          ]
        );
      },
    );
  }
}