import 'package:flutter/material.dart';
import 'package:portfolio/views/passion/passion_content.dart';
import 'package:portfolio/widgets/centered_view.dart';
import 'package:portfolio/widgets/topic_of_view.dart';


class PassionMobileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
          TopicOfView(
          title: 'Passion',
          numberTile: '02',
          fontSize: 30,
        ),

        SizedBox(height: 40,),

        CenteredView(
          horizontalPadding: 40,
          child: Column(
            children:<Widget>[
              PassionContent(
                assetPath: 'assets/design.png',
                description: 'I think that design is like a magic wand. If a service or product is magic to solve '
                  'a problem, it would be the role of the design to mediate it for the user (user) to master it. We '
                  'strive every day to create a "magic wand" that matches the user.',
                title: 'DESIGN',
              ),

              
              SizedBox(height: 20,),

              PassionContent(
                assetPath: 'assets/technology.png',
                description: 'As the phrase "well-developed technology is indistinguishable from magic," '
                  'technology has the power to change the world and the lives of individuals. I want to be a person who '
                  'can always catch up with the latest technology and respond to changes. I think it is also the mission '
                  'of designers as well as engineers to promote innovation.',
                title: 'TECHNOLOGY',
              ),

              SizedBox(height: 20,),
              
              PassionContent(
                assetPath: 'assets/story.png',
                description: "I love stories such as novels, movies, and comics. Stories can attract people's "
                "attention and make things easier to understand and remember. I believe that if you can study all the"
                " story patterns and make your output a story, you'll get a big return.",
                title: 'STORY',
              ),
              
            ]
          ),
        )
      ],
      
    );
  }
}