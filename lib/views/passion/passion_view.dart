import 'package:flutter/material.dart';
import 'package:portfolio/views/passion/passion_desktop.view.dart';
import 'package:portfolio/views/passion/passion_mobile_view.dart';
import 'package:responsive_builder/responsive_builder.dart';

class PassionView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: PassionMobileView(),
      desktop: PassionDesktopView(),
    );
  }
}