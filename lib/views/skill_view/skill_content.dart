import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';


class SkillContent extends StatelessWidget {

  final String skillTitle;
  final String assetPath;
  final double progressValue;
  final String experience;
  final double imageSize;

  SkillContent({this.skillTitle, this.assetPath,
    this.experience, this.progressValue, 
    this.imageSize});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation){
        double linearProgressHeight = sizingInformation.isMobile?7:14;
        double fontSize = sizingInformation.isMobile?12:24;
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(assetPath, width: imageSize,),
            SizedBox(width: 26,),
            Expanded(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      
                      RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: skillTitle, 
                              style: TextStyle(color: Colors.black, fontSize: fontSize, fontWeight: FontWeight.w500, letterSpacing: 4),),
                            TextSpan(text: '  '+experience, 
                              style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w200, fontSize: 12),),
                            
                          ]
                        ),
                      ),
                      RichText(
                        text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(text: '${progressValue *100}', 
                            style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w600, color: Colors.black,),),
                          TextSpan(text: ' %', 
                            style: TextStyle(color: Colors.black, fontSize: 12),),
                          
                        ],
                        ),
                      )
                    ]
                  ),

                  SizedBox(height: 8,),

                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: LinearProgressIndicator(
                        value: progressValue,
                        backgroundColor: Colors.grey[100],
                        minHeight: linearProgressHeight,
                        valueColor:new AlwaysStoppedAnimation<Color>(Colors.lightBlue),
                      )
                  )
                ],
              ) 
            )
          ],
        );
      },
    );
  }
}