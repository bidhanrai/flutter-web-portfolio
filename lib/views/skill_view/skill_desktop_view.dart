import 'package:flutter/material.dart';
import 'package:portfolio/views/skill_view/skill_content.dart';
import 'package:portfolio/widgets/centered_view.dart';
import 'package:portfolio/widgets/topic_of_view.dart';


class SkillDesktopView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopicOfView(
          title: 'SKILL SET',
          numberTile: '03',
          fontSize: 40,
        ),

        SizedBox(height: 100,),

        CenteredView(
          horizontalPadding: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children:<Widget>[
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Column(
                  children: [
                    SkillContent(
                      assetPath: 'assets/flutter.png',
                      experience: '1.5 Years',
                      progressValue: 0.8,
                      skillTitle: 'Flutter',
                      imageSize: 60,

                    ),

                    SizedBox(height: 50,),
                    SkillContent(
                      assetPath: 'assets/react.png',
                      experience: '3 Months',
                      progressValue: 0.2,
                      skillTitle: 'React',
                      imageSize: 60,

                    ),
                    
                    SizedBox(height: 50,),
                    SkillContent(
                      assetPath: 'assets/javascript.png',
                      experience: '6 Months',
                      progressValue: 0.2,
                      skillTitle: 'Javascript',
                      imageSize: 60,

                    ),
                    
                    SizedBox(height: 50,),
                    SkillContent(
                      assetPath: 'assets/htmlcss.png',
                      experience: '1 Year',
                      progressValue: 0.5,
                      skillTitle: 'HTML/CSS',
                      imageSize: 60,
  
                    ),
                  ],
                ),
              ),

              SizedBox(
                width: 60,
              ),
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Column(
                  children: [
                    SkillContent(
                      assetPath: 'assets/python.jpg',
                      experience: '3 Months',
                      progressValue: 0.2,
                      skillTitle: 'Python',
                      imageSize: 60,

                    ),
                    
                    
                    SizedBox(height: 50,),
                    SkillContent(
                      assetPath: 'assets/django.png',
                      experience: '1 Month',
                      progressValue: 0.1,
                      skillTitle: 'Django',
                      imageSize: 60,

                    ),
                    
                    SizedBox(height: 50,),
                    SkillContent(
                      assetPath: 'assets/android.png',
                      experience: '2 Years',
                      progressValue: 0.6,
                      skillTitle: 'Android',
                      imageSize: 60,

                    ),
                    
                    SizedBox(height: 50,),
                    SkillContent(
                      assetPath: 'assets/docker.png',
                      experience: '1 Year',
                      progressValue: 0.5,
                      skillTitle: 'Docker',
                      imageSize: 60,

                    ),
                  ],
                ),
              ),
            ]
          )
        )
      ],
    );
  }
}