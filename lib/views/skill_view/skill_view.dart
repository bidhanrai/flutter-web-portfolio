import 'package:flutter/material.dart';
import 'package:portfolio/views/skill_view/skill_desktop_view.dart';
import 'package:portfolio/views/skill_view/skill_mobile_view.dart';
import 'package:responsive_builder/responsive_builder.dart';


class SkillView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: SkillMobileView(),
      desktop: SkillDesktopView(),
    );
  }
}