import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CenteredView extends StatelessWidget {

  final Widget child;
  final double horizontalPadding;
  CenteredView({this.child, this.horizontalPadding});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      alignment: Alignment.topCenter,
      child: ConstrainedBox(constraints: BoxConstraints(maxWidth: 1200),child: child),
    );
  }
}