import 'package:flutter/material.dart';
import 'package:portfolio/constants/app_color.dart';

class ImageSlider extends StatefulWidget {
  @override
  _ImageSliderState createState() => _ImageSliderState();
}

class _ImageSliderState extends State<ImageSlider> {
  PageController controller = PageController();
  List<String> _images = [
    'assets/me.jpg',
    'assets/e.jpg',
    'assets/computer.jpg'
  ];

  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.bottomCenter, children: [
      SizedBox(
        height: 400,
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
            return true;
          },
          child: PageView.builder(
              controller: controller,
              onPageChanged: (value) {
                setState(() {
                  _current = value;
                });
              },
              itemCount: _images.length,
              itemBuilder: (context, index) {
                return Image.asset(_images[index], semanticLabel: 'Image');
              }),
        ),
      ),
      Positioned(
        top: 175,
        left: 0,
        child: IconButton(
            splashRadius: 1,
            onPressed: () {
              controller.animateToPage(_current - 1,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOut);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white70,
              size: 40,
            )),
      ),
      Positioned(
        top: 175,
        right: 10,
        child: IconButton(
            splashRadius: 1,
            onPressed: () {
              controller.animateToPage(_current + 1,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOut);
            },
            icon: Icon(
              Icons.arrow_forward_ios,
              color: Colors.white70,
              size: 40,
            )),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _images.map((path) {
          int index = _images.indexOf(path);
          return Container(
            width: 8.0,
            height: 8.0,
            padding: const EdgeInsets.all(2),
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
            decoration: BoxDecoration(
              // border: Border.all(color: Colors.pinkAccent),
              shape: BoxShape.circle,
              color: _current == index ? pink : Colors.grey[400],
            ),
          );
        }).toList(),
      )
    ]);
  }
}
