import 'package:flutter/material.dart';
import 'package:portfolio/extensions/hover_extensions.dart';


class NavBarItem extends StatelessWidget {

  final Widget child;
  NavBarItem({this.child});
  @override
  Widget build(BuildContext context) {
    return child.showCursorOnHover.fadeInOutOnHover;
  }
}