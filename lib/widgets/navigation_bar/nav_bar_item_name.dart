import 'package:flutter/material.dart';


class NavBarItemName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Bidhan Rai',style: TextStyle(color:Colors.white, fontSize: 20, letterSpacing: 3),),
          SizedBox(height: 30,),
          Icon(Icons.arrow_back, color: Colors.white,)
        ],
      ),
    );
  }
}