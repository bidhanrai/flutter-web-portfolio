import 'package:flutter/material.dart';
import 'package:portfolio/widgets/navigation_bar/nav_bar_item_name.dart';
import 'package:portfolio/widgets/navigation_drawer/navigation_drawer.dart';

import 'nav_bar_item.dart';

class NavBarMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(40),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            NavBarItemName(),

            InkWell(
              onTap: (){
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => NavigationDrawer()),
                );
              },
              child: NavBarItem(
                child: Container(
                margin: const EdgeInsets.only(left: 20),
                height: 50,
                  width: 50,
                  child: Column(
                    children: [
                      Divider(thickness:3, color: Colors.white),
                      Divider(thickness:3, color: Colors.white,)
                    ]
                  ),
                ),
              ),
            )
          ],
        ),
     ); 
  }
}