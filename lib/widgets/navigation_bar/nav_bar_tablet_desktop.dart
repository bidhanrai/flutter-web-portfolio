import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:portfolio/widgets/launch_url.dart';
import 'package:portfolio/widgets/navigation_bar/nav_bar_item.dart';
import 'package:portfolio/widgets/navigation_bar/nav_bar_item_name.dart';
import 'package:portfolio/widgets/navigation_drawer/navigation_drawer.dart';

class NavBarTabletDesktop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(40),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            NavBarItemName(),
            Wrap(
                  spacing: 34,
                  children: <Widget>[
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.instagram, color: Colors.white,),
                        onTap: ()=>launchUrl('https://www.instagram.com/bidhanrae/'),
                      ),
                    ),
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.facebookSquare, color: Colors.white,),
                        onTap: ()=>launchUrl('https://www.facebook.com/bidhan.sangpang/'),
                      ),
                    ),
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.linkedin, color: Colors.white,),
                        onTap: ()=>launchUrl('https://www.linkedin.com/in/bidhan-rai-382589153/'),
                      ),
                    ),
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.bitbucket, color: Colors.white,),
                        onTap: ()=>launchUrl('https://bitbucket.org/dashboard/projects'),
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context,
                          MaterialPageRoute(builder: (context) => NavigationDrawer()),
                        );
                      },
                      child: NavBarItem(
                        child: Container(
                          margin: const EdgeInsets.only(left: 20),
                          height: 50,
                          width: 50,
                          child: Column(
                            children: [
                              Divider(thickness:3, color: Colors.white),
                              Divider(thickness:3, color: Colors.white,)
                            ]
                          )
                        ),
                      ),
                    ),
                  ],
                )  
          ],
        ),
     );      
  }


}