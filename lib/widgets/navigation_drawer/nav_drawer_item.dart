import 'package:flutter/material.dart';


class NavDrawerItem extends StatelessWidget {

  final String title;
  final double fontSize;
  NavDrawerItem({this.title, this.fontSize});
  @override
  Widget build(BuildContext context) {
    return Text(title, style: TextStyle(fontSize: fontSize, color: Colors.white, fontWeight: FontWeight.w500));
  }
}