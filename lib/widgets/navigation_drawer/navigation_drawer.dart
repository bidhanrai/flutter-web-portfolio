import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:portfolio/constants/app_color.dart';
import 'package:portfolio/widgets/launch_url.dart';
import 'package:portfolio/widgets/navigation_bar/nav_bar_item.dart';
import 'package:portfolio/widgets/navigation_drawer/nav_drawer_item.dart';
import 'package:responsive_builder/responsive_builder.dart';

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation){
        var fontSize = sizingInformation.isMobile?50.0: sizingInformation.isTablet?80: 100.0;
        var mainAxisalignmentValue = sizingInformation.isMobile?MainAxisAlignment.center:MainAxisAlignment.spaceAround;
        var sizeBoxHeight =  sizingInformation.isMobile?30.0: 0.0;
        return Scaffold(
          body: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: new LinearGradient(
                colors: [purple, pink],
                begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  tileMode: TileMode.repeated
                ),
            ),
            child: Column(
              mainAxisAlignment: mainAxisalignmentValue,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                NavDrawerItem(title: 'HOME',fontSize: fontSize,),
                SizedBox(height: sizeBoxHeight,),
                NavDrawerItem(title: 'WHO I AM',fontSize: fontSize,),
                SizedBox(height: sizeBoxHeight,),
                NavDrawerItem(title: 'WHAT I DO',fontSize: fontSize,),
                SizedBox(height: sizeBoxHeight,),
                NavDrawerItem(title: 'CONTACT',fontSize: fontSize,),
                SizedBox(height: sizeBoxHeight,),
                sizingInformation.isMobile? 
                  Wrap(
                    spacing: 20,
                    children: [
               NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.instagram, color: Colors.white,),
                        onTap: ()=>launchUrl('https://www.instagram.com/bidhanrae/'),
                      ),
                    ),
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.facebookSquare, color: Colors.white,),
                        onTap: ()=>launchUrl('https://www.facebook.com/bidhan.sangpang/'),
                      ),
                    ),
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.linkedin, color: Colors.white,),
                        onTap: ()=>launchUrl('https://www.linkedin.com/in/bidhan-rai-382589153/'),
                      ),
                    ),
                    NavBarItem(
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.bitbucket, color: Colors.white,),
                        onTap: ()=>launchUrl('https://bitbucket.org/dashboard/projects'),
                      ),
                    ),
                    ],
                  ):
                  SizedBox(),
                
              ]
            )
          )
        );
      }
    );
  }
}