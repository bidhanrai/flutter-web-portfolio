import 'package:flutter/material.dart';
import 'package:portfolio/constants/app_color.dart';
import 'package:responsive_builder/responsive_builder.dart';

class TopicOfView extends StatelessWidget {

  final String title;
  final String numberTile;
  final double fontSize;
  TopicOfView({this.numberTile, this.title,this.fontSize});

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation){
        return Row(
          children: [
            SizedBox(
              width: sizingInformation.isMobile?30:50,
              child: Divider(
                thickness: 2,
                color: Colors.black,
              )
            ),
            SizedBox(width: sizingInformation.isMobile?10:20),
            Text(numberTile, style: TextStyle(color: pink, fontSize: fontSize, fontWeight: FontWeight.bold),),
            SizedBox(width: sizingInformation.isMobile?40:50),
            Expanded(child: Text(title, style: TextStyle(letterSpacing: 2, wordSpacing: 4, fontSize: fontSize, color: Colors.black),),),

          ],      
        );
      }
    );
  }
}